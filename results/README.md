﻿# Data exploration

The results shown below represent just a few examples of the initial graphical and tabular summaries produced to address the set research questions articulated on the [analysis page](../code/analysis/). For a complete set of results, please refer to the [**figures/**](figures/) and [**tables/**](tables/) directories above.


## Sampling effort distribution
A [table](tables/sampling_effort_distribution.md) showing the number of plots sampled in each study group by sampling event (the year a given sample was collected relative to date during which baseline data was collected).


## An example forest structural variable (i.e., the density of live trees)
### Live tree density by group for the pre-treatment and all post-treatment sampling events
![alt text](figures/forest_structure/trees_per_ha_boxplot.png)
### The distribution of live tree density by sampling event
![alt text](figures/forest_structure/trees_per_ha_histogram.png)


## An example surface fuel attribute (i.e., counts of fuels in the 100-hour category)
### 100-hour fuel counts by group for the pre-treatment and all post-treatment sampling events
![alt text](figures/fine_fuels/hundred_hour_counts_boxplot.png)
### The distribution of 100-hour fuel counts by sampling event
![alt text](figures/fine_fuels/hundred_hour_counts_boxplot.png)
