

|  Group  |      Variable       |  Baseline  |  Year 20  |  Direction  |
|:-------:|:-------------------:|:----------:|:---------:|:-----------:|
|  Burn   |    one_hour_vol     |    2.8     |   1.09    |    Down     |
| Control |    one_hour_vol     |    2.17    |     1     |    Down     |
|  Burn   |    ten_hour_vol     |   13.78    |   10.84   |    Down     |
| Control |    ten_hour_vol     |   13.94    |   7.88    |    Down     |
|  Burn   |  hundred_hour_vol   |   16.17    |   20.31   |     Up      |
| Control |  hundred_hour_vol   |   18.54    |   19.75   |     Up      |
|  Burn   | total_fine_fuel_vol |   32.74    |   32.24   |    Down     |
| Control | total_fine_fuel_vol |   34.65    |   28.63   |    Down     |

