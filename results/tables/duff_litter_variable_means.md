

|  Group  |     Variable     |  Baseline  |  Year 20  |  Direction  |
|:-------:|:----------------:|:----------:|:---------:|:-----------:|
|  Burn   |  duff_depth_cm   |    6.57    |   2.07    |    Down     |
| Control |  duff_depth_cm   |    6.7     |   4.74    |    Down     |
|  Burn   | litter_depth_cm  |    7.58    |   3.66    |    Down     |
| Control | litter_depth_cm  |    6.98    |   4.76    |    Down     |
|  Burn   | fuelbed_depth_cm |   14.15    |   5.73    |    Down     |
| Control | fuelbed_depth_cm |   13.68    |    9.5    |    Down     |

