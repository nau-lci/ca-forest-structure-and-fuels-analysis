

|  Group  |     Variable      |  Baseline  |  Year 20  |  Direction  |
|:-------:|:-----------------:|:----------:|:---------:|:-----------:|
|  Burn   | thousand_hour_vol |   94.53    |   356.5   |     Up      |
| Control | thousand_hour_vol |   125.7    |   393.2   |     Up      |

