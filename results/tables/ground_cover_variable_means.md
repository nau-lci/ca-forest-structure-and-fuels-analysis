

|  Group  |          Variable          |  Baseline  |  Year 20  |  Direction  |
|:-------:|:--------------------------:|:----------:|:---------:|:-----------:|
|  Burn   | bare_ground_and_rock_cover |    0.03    |   0.07    |     Up      |
| Control | bare_ground_and_rock_cover |    0.02    |   0.01    |    Down     |
|  Burn   |        litter_cover        |    0.81    |   0.43    |    Down     |
| Control |        litter_cover        |    0.68    |   0.49    |    Down     |
|  Burn   |        woody_cover         |    0.02    |   0.21    |     Up      |
| Control |        woody_cover         |    0.05    |   0.24    |     Up      |
|  Burn   |         live_cover         |    0.13    |    0.3    |     Up      |
| Control |         live_cover         |    0.26    |   0.26    |     Up      |
|  Burn   |      species_richness      |    2.75    |   3.44    |     Up      |
| Control |      species_richness      |    4.6     |     3     |    Down     |
|  Burn   |      species_evenness      |    0.74    |   0.62    |    Down     |
| Control |      species_evenness      |    0.74    |   0.73    |    Down     |

