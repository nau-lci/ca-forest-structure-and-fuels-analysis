

|  Group  |           Variable            |  Baseline  |  Year 20  |  Direction  |
|:-------:|:-----------------------------:|:----------:|:---------:|:-----------:|
|  Burn   |       live_trees_per_ha       |   526.1    |   265.6   |    Down     |
| Control |       live_trees_per_ha       |    572     |   481.5   |    Down     |
|  Burn   |         snags_per_ha          |   227.9    |   98.33   |    Down     |
| Control |         snags_per_ha          |   189.2    |   155.4   |    Down     |
|  Burn   |      large_trees_per_ha       |   41.07    |   38.89   |    Down     |
| Control |      large_trees_per_ha       |    22.4    |    30     |     Up      |
|  Burn   |      small_trees_per_ha       |    325     |    80     |    Down     |
| Control |      small_trees_per_ha       |   342.8    |   248.5   |    Down     |
|  Burn   | spotted_owl_nest_trees_per_ha |   23.57    |   23.89   |     Up      |
| Control | spotted_owl_nest_trees_per_ha |    8.4     |   13.08   |     Up      |
|  Burn   |     proportion_white_fir      |    0.69    |   0.67    |    Down     |
| Control |     proportion_white_fir      |    0.69    |   0.73    |     Up      |
|  Burn   |        proportion_pine        |    0.18    |   0.23    |     Up      |
| Control |        proportion_pine        |    0.22    |   0.16    |    Down     |
|  Burn   |   proportion_shade_tolerant   |    0.77    |   0.71    |    Down     |
| Control |   proportion_shade_tolerant   |    0.75    |   0.82    |     Up      |
|  Burn   |  proportion_shade_intolerant  |    0.19    |   0.24    |     Up      |
| Control |  proportion_shade_intolerant  |    0.24    |   0.18    |    Down     |
|  Burn   |         live_tree_qmd         |   37.83    |   47.4    |     Up      |
| Control |         live_tree_qmd         |   34.23    |   38.82   |     Up      |

