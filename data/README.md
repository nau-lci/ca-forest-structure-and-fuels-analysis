# Contents

CSVs in the parent directory contain forest structure, surface fuel, and community composition data exported from [FFI](https://www.frames.gov/partner-sites/ffi/ffi-home/), a software tool for ecological monitoring. Exports � and even a few direct changes to the database based on our QA/QC process � were made by Daniel Shaw (daniel.shaw@parks.ca.gov). For more detailed information about specific changes and decisions, please consult the [issues page](https://bitbucket.org/nau-lci/ca-forest-structure-and-fuels-analysis/issues) of the repository.

*ffi_nonspatialdb.backup* is a PostgreSQL backup file that can be used to restore the self-contained database created specifically for this analysis. The steps used to create the database locally can be found in the [**config/**](../config/) directory of the repository. The data directory includes the two additional child directories, described below.

Directory                 		| Description
----------------------------------------|-------------------------------------------------------------------------------------------------------
[**developed/**](developed/)         	| CSVs containing data summarized by the year of study and plot 
[**known_issues/**](known_issues/)      | A collection of known issues in the data, some of which are corrected in the process of generating the CSVs seen in the [**developed/**](developed/) directory. Issues are described at length in the known issues README. 
