﻿# California forest structure and fuels analysis

This repository contains code, data, documents, metadata, and results associated with an analysis of forest 
structure and surface fuel change over a 20-year period in the Lake Tahoe Basin. The work was conducted by 
the [Lab of Landscape Ecology and Conservation Biology](http://www.nau.edu/LCI/Research/) at Northern 
Arizona University under contract with [California Department of Parks and Recreation](http://www.parks.ca.gov). 
Files are organized according to the directory structure outlined in the following table. 

Directory            | Description
---------------------|--------------------------------------------------------------------------
[**code/**](code/)        | Code (Python, R, batch files, etc.) used to prepare, explore, and analyze data
[**config/**](config/)| A guide to the forest structure and fuels database, including software installation and configuration instructions
[**data/**](data/)        | Data used to populate the relational database developed for this analysis, as well as data prepared (i.e., aggregated) for analysis, and any known issues
[**docs/**](docs/)        | Project-related documents, including key references, links to reporting materials, and research questions
[**results/**](results/)  | Tabular and graphical results of analyses
 

## Contact information

Please contact Luke Zachmann (luke.zachmann@nau.edu) with any questions or concerns.