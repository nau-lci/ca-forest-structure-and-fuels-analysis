source(file.path(PROJHOME, 'code', 'analysis', 'misc', 'ar_utils.R'))
# http://stackoverflow.com/questions/22504682/how-to-fit-autoregressive-poisson-mixed-model-count-time-series-in-r

set.seed(12345) # updated to T=20 and L=40 for comparative purposes.

T = 20 # number of years
L = 40  # number of sites
N0 = 100 # average initial pop (to simulate data)
sd_env = 0.8 # to simulate the env (assumed mean 0)
env  = matrix(rnorm(T*L, mean=0, sd=sd_env), nrow=T, ncol=L)

# 'real' parameters
alpha  = 0.1
beta   = 0.05
sd     = 0.4
gamma  = rnorm(T-1, mean=0, sd=sd)
mu_ini = log(rpois(n=L, lambda=N0)) #initial means

par_real = list(alpha=alpha, beta=beta, gamma=gamma,
                sd=sd, mu_ini=mu_ini)

mu = dynamics(par=par_real, x=env, T=T, L=L)

# observed abundances
n = matrix(rpois(length(mu), lambda=mu), nrow=T, ncol=L)

# ==============================================================================

library(dplyr)
library(ggplot2)


nn <- n %>%
  as.data.frame %>%
  reshape2::melt() %>%
  group_by(variable) %>%
  mutate(time=1:T) %>%
  rowwise %>%
  mutate(variable=as.numeric(sub('V', '', variable))) %>%
  ungroup

ggplot(nn %>% filter(variable %in% sample(1:L, 5))) +
  geom_line(aes(x=time, y=value, group=variable, color=factor(variable)))


# ==============================================================================

library(rstan)

nnn <- nn  # %>% filter(variable<=4)
stan_data <- list(N=nrow(nnn),
                  J=n_distinct(nnn[['variable']]),
                  #K=1,
                  timesteps=n_distinct(nnn[['time']]),
                  site=nnn[['variable']],
                  T=nnn[['time']],
                  Y=nnn[['value']]
)

stan_code <- '
data {
int<lower=0> N;
int<lower=0> J;
int timesteps;

int<lower=1, upper=J> site[N];
int<lower=0> T[N];

int Y[N];
}
parameters {
// Group-level effects
//real u_0j[J];
// Standard deviation of group-level effect u_0j
//real<lower=0> sigma_u0;

real beta_0;          // intercept
real<lower=0> rho;    // autoregression parameter
real<lower=0> sigma;  // noise scale on latent state evolution
vector[timesteps] u;  // latent state ("error term")
}

// Parameters for the varying intercepts
//real beta_0j[J];
model {

// priors
beta_0 ~ normal(0, 5);
rho ~ lognormal(0, 2);
sigma ~ lognormal(0, 2);

// latent state
u[1] ~ normal(0, 5);
for (t in 2:timesteps)
u[t] ~ normal(rho * u[t - 1], sigma);

// Varying intercepts definition
//for (j in 1:J)
//beta_0j[j] = beta_0 + u_0j[j];

// likelihood
for (n in 1:N)
Y[n] ~ poisson_log(beta_0 + u[T][n]);  // u[n] is the problem!!!
}'

this_fit <- stan(model_code=stan_code, data=stan_data, chains=4,
     iter=15000, warmup=3000, cores=4,
     control=list(stepsize=0.01, adapt_delta=0.95), seed=96161)


these_params <- c('beta_0', 'rho', 'sigma')
print(this_fit, pars = these_params)

this_fit_samples <- rstan::extract(this_fit, pars = these_params) %>%
  as.data.frame %>%
  reshape2::melt()
ggplot(this_fit_samples, aes(x=value)) +
  geom_density(fill='yellow', alpha=0.3) +
  facet_wrap(~variable, scales='free')

traceplot(this_fit, pars = these_params, inc_warmup = FALSE,
          window=c(7000, 7200)) +
  scale_x_continuous(breaks=c(7000, 7100, 7200))
