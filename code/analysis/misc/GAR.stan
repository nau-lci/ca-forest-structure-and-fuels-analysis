data {
  int<lower=1> T;       // number of observations
  int y[T];             // observed outputs
}
parameters {
  real mu;            
  real<lower=0,upper=1> phi;         
  real<lower=0> sigma;  
  vector[T] nu_z;  
}
model {
  vector[T] nu;  
  mu ~ normal(0,2);
  sigma ~ normal(0,1);
  nu[1] <- mu / (1 - phi) + sigma / sqrt(1 - square(phi)) * nu_z[1];
  for (t in 2:T)  nu[t] <- mu + phi * nu[t-1] + sigma * nu_z[t];
  nu_z ~ normal(0,1);
  y ~ poisson_log(nu);
}