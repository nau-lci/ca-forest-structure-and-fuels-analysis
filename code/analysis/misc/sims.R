library(dplyr)

n <- 1000
year <- c(0, 20)
group <- 1:n

df <- data.frame(group=rep(group, 2), year=rep(year, each=n)) %>%
  rowwise() %>%
  mutate(year_effect = ifelse(year==0, rnorm(1, 10, 5), NA),
         response = ifelse(year==0, rnorm(1, year_effect, 20), NA)) %>%
  ungroup() %>%
  group_by(group) %>%
  mutate(response = ifelse(year==20, rnorm(1, year_effect*5, 5), response))

plot(df$response[year==20]~df$response[year==0])
plot(response~factor(year), data=df)

sim_mod_1 <- lm(response~factor(year), data=df)
summary(sim_mod_1)
coef(sim_mod_1)
# y_new <-
  coef(sim_mod_1)['factor(year)20'] * 1 + coef(sim_mod_1)['(Intercept)']

alt_df <- df %>%
  filter(year==20)
alt_df$baseline <- df %>% filter(year==0) %>% .[['response']]
sim_mod_2 <- lm(response~baseline, data=alt_df)
summary(sim_mod_2)
coef(sim_mod_2)
# y_new <-
  mean(coef(sim_mod_2)['baseline'] * alt_df$baseline + coef(sim_mod_2)['(Intercept)'])
  coef(sim_mod_2)['baseline'] * mean(alt_df$baseline) + coef(sim_mod_2)['(Intercept)']  # better!
  alt_df %>% ungroup %>% summarise(mean_baseline=mean(baseline))

coef(sim_mod_1)
coef(sim_mod_2)
