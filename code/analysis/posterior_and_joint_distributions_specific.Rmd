---
title: "Appendix A"
author: "Luke Zachmann"
date: "`r gsub(' 0', ' ', format(Sys.time(), '%B %d, %Y'))`"
bibliography: bibliography.bib
csl: forest-ecology-and-management.csl
output: 
  html_document:
    theme: journal
    highlight: default
    smart: true
---

### Specific models
#### An example involving tree density

In the case of tree density, counts of live trees, $y_{ij}$, were obtained
on each of the $j=1, ..., m$ plots. The model linking the variable of interest (tree density, $\lambda_i$) to data is

$$ y_{ij} \sim \textrm{Poisson}(\lambda_i).$$

We chose the Poisson distribution because the data form counts. The Poisson distribution has a single parameter, $\lambda$, the average number of occurrences. Note that the parameter for sampling variance, $\sigma^2$, specified in the general model (Figure 2 and Eq. 3 in the manuscript) is missing. In the Poisson distribution, the first and second central moments are equal. Had we assumed a negative binomial likelihood, $\sigma^2$ would have made an appearance here. The process model representing the effect of time, treatment, and their interaction is
is

$$log(\lambda_i) = g(\nu_j, \boldsymbol{\beta}, \boldsymbol{x}_{ij}) = \nu_j + \beta_1x_{1,ij} + \beta_2x_{2,ij} + ... + \beta_kx_{k,ij}.$$

The log-link is used to map $\lambda_i$ from its constrained space [0, $\infty$) to the unconstrained space ($-\infty$, $\infty$). All of the covariates, $x_{1,ij}, ..., x_{k,ij}$, are described in Table 2 of the manuscript. Because variation among plots is not represented in our process model, we included that variation by modeling the intercept as

$$\nu_j \sim normal(\alpha, \varsigma^2).$$

In this case, we allow each plot to have its own mean drawn from a distribution of means with hyperparameters $\alpha$ and $\varsigma^2$, the mean of means and variance of means, respectively. In other words, the effect of plot varies randomly according to sources of variation that we acknowledge exist, but do not attempt to explain. Thus, the expression for the posterior and joint distributions is

\[
\begin{aligned}
  ~[\alpha, \varsigma^2, \boldsymbol{\nu}, \boldsymbol{\beta}~|~\boldsymbol{y}] &∝
  \prod_{j=1}^{m}\prod_{i=1}^{n}\textrm{Poisson}(y_{ij}~|~e^{g(\nu_j, \boldsymbol{\beta}, \boldsymbol{x}_{ij})}) \\
  &\times~\textrm{normal}(\nu_j~|~\alpha, \varsigma^2) \\
  &\times~\textrm{normal}(\alpha~|~0, 10) \\
  &\times~\textrm{half-Cauchy}(\varsigma^2~|~0, 5) \\
  &\times~\prod_{p=1}^{k}\textrm{normal}(\beta_p~|~0, 10).
\end{aligned}
\]
<br>

#### An example involving the proportion of small trees
As with the previous example, attributes related to forest composition were originally collected as counts. But in this case, the counts represent the number of _successes_ -- the number of small trees (15-30 cm DBH) -- observed in a given number of _trials_ -- the total number of trees -- on a plot. The approach we took to model this data was

$$y_{ij} \sim \textrm{binomial}(z_{ij}, \phi),$$

which says that the number of small trees on the $j^{th}$ plot is represented as a random variable $y_{ij}$ drawn from a binomial distribution where $z_{ij}$ is the total number of trees (>15 cm DBH) on plot $j$ during sampling even $i$ and $\phi$ is the probability of observing a shade-tolerant tree. Our primary interest is the variation in the proportion of small trees among plots, particularly the variation contributed by treatment, time, and their interaction. As such, we model $\phi$ as

$$\textrm{logit}(\phi) \sim \textrm{normal}(g(\nu_j, \boldsymbol{\beta}, \boldsymbol{x}_{ij}), \sigma^2),$$

where the process model, $g(\nu_j, \boldsymbol{\beta}, \boldsymbol{x}_i)$, as well as the set of covariates, $x_{1,ij}, ..., x_{k,ij}$, are the same as described in the previous example. We tranform $\phi$ so that it takes on values appropriate for the normal distribution ($-\infty$, $\infty$). And group-level effects for $\nu_j$ are the same as define above. The expression for the posterior and joint distributions is

\[
\begin{aligned}
  ~[\phi, \alpha, \varsigma^2, \boldsymbol{\nu}, \boldsymbol{\beta}~|~\boldsymbol{y}] &∝
  \prod_{i=1}^{n}\textrm{binomial}(y_{ij}~|~z_{ij}, \phi) \\
  &\times~\textrm{normal}(\textrm{logit}(\phi)~|~g(\nu_j, \boldsymbol{\beta}, \boldsymbol{x}_i), \sigma^2) \\
  &\times~\textrm{normal}(\alpha~|~0, 10) \\
  &\times~\textrm{half-Cauchy}(\varsigma^2~|~0, 5) \\
  &\times~\prod_{p=1}^{k}\textrm{normal}(\beta_p~|~0, 10) \\
  &\times~\textrm{half-Cauchy}(\sigma^2~|~0, 50)
\end{aligned}
\]

where $\boldsymbol{z}$ is not included in the posterior distribution because we assume it is known. Indeed, as specified, $\boldsymbol{z}$ is part of the data.

<!-- # References -->
