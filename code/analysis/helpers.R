library(ggplot2)
library(gridExtra)
source(file.path(PROJHOME, 'code', 'data_exploration', 'utils.R'))


# Classes.
setClass('model_config', representation(
  dependent_var = 'character',
  model_desc = 'character',
  term_labels = 'character',
  family = 'character',
  trials_var = 'character',
  transformation = 'numeric'
  ))


# General-purpose wrappers.
pipe_assign <- function(data, x, value) return(assign(x, value, pos = 1))
slice_list <- function(x, slice=1) x[[slice]]
log10_ceiling <- function(x) 10^(ceiling(log10(x)))
decimal_places <- Vectorize(function(x) {
  if ((x %% 1) != 0) {
    nchar(strsplit(sub('0+$', '', format(x, scientific=FALSE)), ".", fixed=TRUE)[[1]][[2]])
  } else {
    return(0)
  }
})
rf_d <- function(x, d=NULL) {
  if(is.null(d)) d <- decimal_places(log10_ceiling(abs(x)) / 100)
  return(format(round(x, d), nsmall = d))
}
wrap_lab <- function(x, ...) paste(strwrap(x, ...), collapse = "\n")


# Misc.
bci_lower <- function(x) quantile(x, probs=c(0.025))
bci_upper <- function(x) quantile(x, probs=c(0.975))


# Wrappers for model evaluation.
parse_random_effects_slot <- function(random_effects) {
  unique(unlist(sapply(random_effects, function(x) strsplit(x, ':'))))
}
sig_fixed_effects <- function(results_container, model) {
  # Test if the BCI for the fixed effects overlaps zero.
  sig_effects_list <- lapply(model, function(x) {
    # browser()
    this_result <- results_container[[x]]
    this_model <- this_result$stan_fit
    these_params <- this_result$stan_pars
    fit_summary <- summary(this_model, pars = these_params)
    all_fixed_effects <- fit_summary$summary
    mutate_call <- lazyeval::interp(~0>a & 0<b, a = as.name('2.5%'),
                                    b = as.name('97.5%'))
    # all_fixed_effects <-
    #
    #   fixef(this_model, estimate=c('mean', 'quantile'), probs=c(0.025, 0.975)) %>%
    #   as.data.frame
    all_fixed_effects %>%
      as.data.frame %>%
      mutate(fixef=row.names(.)) %>%
      mutate_(.dots=setNames(list(mutate_call), 'contains_zero')) %>%
      # filter(!contains_zero, fixef!='Intercept') %>%
      mutate(dependent_var=results_container[['dependent_var']],
             family=results_container[['family']],
             model=x)
  })
  sig_effects_list %>% bind_rows
}
lm_eqn <- function(m) {
  eq <- substitute(italic(y) == a + b %.% italic(x)*","~~italic(r)^2~"="~r2,
                   list(a = format(coef(m)[1], digits = 3),
                        b = format(coef(m)[2], digits = 3),
                        r2 = format(summary(m)$r.squared, digits = 3)))
  as.character(as.expression(eq))
}
op_regressions <-
  function(results_container, model,
           dir=file.path(PROJHOME, 'results', 'figures', 'model_evaluation',
                         'op_regressions')) {
    lapply(model, function(x) {
      this_model <- results_container[[x]]
      op_df <- data.frame(fitted=predict(this_model, re_formula=NA)[, 'Estimate'],
                          observed=this_model$data[, results_container$dependent_var])
      op_lm <- lm(observed ~ fitted, data=op_df)
      op_lm_coefs <- op_lm$coefficients
      p <- ggplot(op_df, aes_string(x='fitted', y='observed')) +
        geom_abline(intercept = 0, slope = 1, color='red', size=1.05) +
        geom_abline(intercept = op_lm_coefs['(Intercept)'],
                    slope=op_lm_coefs['fitted'], color='blue', size=1.05) +
        geom_point() +
        labs(x=parse(text=paste('Predicted',
                                tolower(results_container$description), sep='~')),
             y=parse(text=paste('Observed',
                                tolower(results_container$description), sep='~'))) +
        geom_text(aes(x=Inf, y=Inf, hjust=1.5, vjust=1.3,
                      label = lm_eqn(op_lm)), parse = TRUE, size=5) +
        # coord_fixed() +
        theme_spec
      png_name <-
        paste0(results_container$dependent_var, '_', x, '.png', sep='')
      ggsave(file.path(dir, png_name), p, width=5, height=5)
    })
    data.frame(NULL)
  }


# Wrappers for creating plots of the posterior predictive distributions.
get_ppd_mcmc <- function(x, posterior_samples, scenario) {
  x_vec <- unlist(x[names(x) != 'scenario'])
  posterior_samples_mat <- as.matrix(posterior_samples)
  data.frame(mu_k=apply(posterior_samples_mat, 1,
                        function(row_vals) sum(row_vals * x_vec)),
             scenario=x$scenario)
}
back_out <- function(x, family) {
  if (family %in% c('lognormal', 'poisson', 'negbinomial')) {
    exp(x)
  } else if (family %in% c('beta', 'binomial')) {
    exp(x)/(1+exp(x))
  }
}
get_just_fixed_effects <- function(model) {
  formula_as_char <- as.character(model$formula)
  left_hand <- formula_as_char[2]
  if(model$family[['family']] == 'binomial')
    left_hand <- strsplit(left_hand, ' \\|')[[1]][1]
  fixef_terms <- paste(left_hand,
                       gsub('\\(1 \\| plot_type:plot_name\\) \\+ ', '', formula_as_char[3]),
                       sep=' ~ ')
  as.formula(fixef_terms)
}
descale <- function(x, scaled_atts) {
  x * scaled_atts[['scaled.scale']] + scaled_atts[['scaled.center']]
}
get_posterior_mu <- function(data, posterior_samples, family) {
  X <- as.matrix(as.data.frame(data[names(data) != 'scenario']))
  if (family != 'binomial') {
    vals <- exp(X %*% t(posterior_samples[, 1:ncol(X)]))
  } else {
    vals <- plogis(X %*% t(posterior_samples[, 1:ncol(X)]))
  }
  data.frame(scenario=data$scenario, mu_k=as.vector(vals))
}
mod_1_scenarios <- function(data) {
  group_means <- data %>%
    select(plot_type) %>%
    group_by(plot_type) %>%
    summarise_each(funs(mean))
  scenarios <- data %>% select(plot_type, sample_year) %>% distinct
  group_means %>% left_join(scenarios)
}
mod_2_scenarios <- function(data) {
  data %>%
    mutate(severity=mean(severity)) %>%
    select(severity, sample_year) %>%
    distinct
}
get_posterior_samples <- function(model, parameters) {
  rstan::extract(model, pars=parameters) %>%
    as.data.frame %>%
    as.matrix
}
generate_ppd_data <- function(results_container) {
  # Model 1.
  mod_1 <- results_container[['mod_1']]$stan_fit
  stan_data_as_df <- results_container[['mod_1']]$stan_data %>%
    as.data.frame
  test <- stan_data_as_df %>%
    bind_cols(results_container[['mod_1']]$data %>% select(plot_type, sample_year))
  mod_1_specs <- mod_1_scenarios(test)
  mod_1_posts <-
    get_posterior_samples(mod_1, results_container[['mod_1']]$stan_pars)
  mod_1_terms <- results_container[['mod_1']]$model_terms
  mod_1_X <- model.matrix(mod_1_terms, model.frame(mod_1_terms, mod_1_specs)) %>%
    as.data.frame
  mod_1_marginal_posteriors <- mod_1_X %>%
    mutate(scenario=1:n()) %>%
    rowwise %>%
    do(get_posterior_mu(., mod_1_posts, results_container[['mod_1']]$family))
  mod_1_specs_info <- mod_1_specs %>%
    mutate(scenario=1:n()) %>%
    rowwise %>%
    select(scenario, plot_type, sample_year)

  # Model 2.
  # browser()
  mod_2 <- results_container[['mod_2']]$stan_fit
  stan_data_as_df <- results_container[['mod_2']]$stan_data %>%
    as.data.frame
  test <- stan_data_as_df %>%
    bind_cols(results_container[['mod_2']]$data %>% select(sample_year))
  mod_2_specs <- mod_2_scenarios(test)
  # browser()
  mod_2_posts <-
    get_posterior_samples(mod_2, results_container[['mod_2']]$stan_pars)
  mod_2_terms <- results_container[['mod_2']]$model_terms
  mod_2_X <- model.matrix(mod_2_terms, model.frame(mod_2_terms, mod_2_specs)) %>%
    as.data.frame
  mod_2_marginal_posteriors <- mod_2_X %>%
    mutate(scenario=1:n()) %>%
    rowwise %>%
    do(get_posterior_mu(., mod_2_posts, results_container[['mod_2']]$family))
  mod_2_specs_info <- mod_2_specs %>%
    mutate(scenario=1:n())

  marginal_posteriors <- mod_1_marginal_posteriors %>%
    ungroup %>%
    left_join(mod_1_specs_info) %>%
    mutate(question=ifelse(scenario %in% c(1, 2, 3), 1, 2))

  return(list(marginal_posteriors=marginal_posteriors,
              marginal_posteriors_q4=mod_2_marginal_posteriors %>%
                left_join(mod_2_specs_info),
              label=results_container$description,
              variable=results_container$dependent_var))
}

