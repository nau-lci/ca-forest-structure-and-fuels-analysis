data {
  // Scalars
  int<lower=1> N;  // number of observations
  int<lower=1> J;  // number of groups
  int<lower=1> K;  // number of covariates, excluding the intercept term

  // Predictors
  //real severity[N];
  int<lower=0, upper=1> plot_typeB[N];
  int<lower=0, upper=1> sample_year15[N];
  int<lower=0, upper=1> sample_year20[N];
  int<lower=1, upper=J> plot_name[N];

  // Response (a count outcome)
  int<lower=0> Y[N];
}
parameters {
  // Group-level effects
  real u_0j[J];
  // Standard deviation of group-level effect u_0j
  real<lower=0> sigma_u0;

  // Population intercept
  real beta_0;
  // Population effects
  real beta[K];

  // Overdispersion parameter
  real<lower=0> phi;
}
transformed parameters  {
  // Parameters for the varying intercepts
  real beta_0j[J];

  // Parameters for the individual mean
  real lp[N];
  real mu[N];

  // Varying intercepts definition
  for (j in 1:J) {
    beta_0j[j] = beta_0 + u_0j[j];
  }

  // Individual mean definition
  for (n in 1:N) {
    // Linear predictor
    lp[n] = beta_0j[plot_name[n]] + beta[1]*plot_typeB[n] + beta[2]*sample_year15[n] + beta[3]*sample_year20[n] + beta[4]*plot_typeB[n]*sample_year15[n] + beta[5]*plot_typeB[n]*sample_year20[n];
    // Mean
    mu[n] = exp(lp[n]);
  }
}
model {
  // Priors
  beta ~ normal(0, 10);

  // Random effects distribution
  u_0j ~ normal(0, sigma_u0);

  // Likelihood
  Y ~ neg_binomial_2(mu, phi);
}
