data {
  // Scalars
  int<lower=1> N;  // number of observations
  int<lower=1> J;  // number of groups
  int<lower=1> K;  // number of covariates, excluding the intercept term

  // Predictors
  int<lower=0, upper=1> plot_typeB[N];
  int<lower=0, upper=1> sample_year15[N];
  int<lower=0, upper=1> sample_year20[N];
  int<lower=1, upper=J> plot_name[N];

  // Response (a non-negative continuous random variable)
  real<lower=0> Y[N];
}
parameters {
  // Population intercept
  real beta_0;
  // Population effects
  real beta[K];

  // Error (standard deviation)
  real<lower=0> sigma_e;
}
model {
  // Mean
  real mu[N];

  // Priors
  beta ~ normal(0, 10);

  // Likelihood
  for (n in 1:N) {
    mu[n] = beta_0 + beta[1]*plot_typeB[n] + beta[2]*sample_year15[n] + beta[3]*sample_year20[n] + beta[4]*plot_typeB[n]*sample_year15[n] + beta[5]*plot_typeB[n]*sample_year20[n];
    Y[n] ~ lognormal(mu[n], sigma_e);
  }
}
