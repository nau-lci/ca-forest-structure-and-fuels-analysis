data {
  // Scalars
  int<lower=1> N;  // number of observations
  int<lower=1> J;  // number of groups
  int<lower=1> K;  // number of covariates, excluding the intercept term

  // Predictors
  int<lower=0, upper=1> plot_typeB[N];
  int<lower=0, upper=1> sample_year15[N];
  int<lower=0, upper=1> sample_year20[N];
  int<lower=1, upper=J> plot_name[N];

  // Response (a non-negative continuous random variable)
  real<lower=0> Y[N];
}
parameters {
  // Group-level effects
  real u_0j[J];
  // Standard deviation of group-level effect u_0j
  real<lower=0> sigma_u0;

  // Population intercept
  real beta_0;
  // Population effects
  real betas[K];

  // Variance parameter
  real phi;
}
transformed parameters  {
  vector[N] mu;  // the expected values (linear predictor)
  vector<lower=0>[N] alpha;  // shape parameter for the gamma distribution
  vector[N] beta;  // rate parameter for the gamma distribution


  // Parameters for the varying intercepts
  real beta_0j[J];

  // Varying intercepts definition
  for (j in 1:J) {
    beta_0j[j] = beta_0 + u_0j[j];
  }

  // Likelihood
  for (n in 1:N) {
    mu[n] = exp(beta_0j[plot_name[n]] + betas[1]*plot_typeB[n] + betas[2]*sample_year15[n] + betas[3]*sample_year20[n] + betas[4]*plot_typeB[n]*sample_year15[n] + betas[5]*plot_typeB[n]*sample_year20[n]);
    alpha[n] = (mu[n] * mu[n]) / phi;
    beta[n] = mu[n] / phi;
  }


}
model {
  // Priors
  betas ~ normal(0, 10);

  // Random effects distribution
  u_0j ~ normal(0, sigma_u0);

  Y ~ gamma(alpha, beta);
}
