---
title: A bestiary of descriptive statistics for the CA State Parks analysis
output: 
  html_document:
    toc: true
---

<!-- http://www.w3schools.com/css/css_table.asp -->
<style>
  th {
    background-color: #000000;
    color: #ffffff;
  }
  tr:nth-child(even) {
      background-color: #d9d9d9;
  }
  tr:hover {background-color: #ffffb2}
  table {
    border-top: 2px solid #737373;
    border-bottom: 2px solid #737373;
  }
</style>

```{r setup, include=FALSE}
library(dplyr)
library(pander)
library(htmlTable)

source(file.path(PROJHOME, 'code', 'analysis', 'load_developed_datasets.R'))
source(file.path(PROJHOME, 'code', 'analysis', 'analysis_dictionary.R'))

knitr::opts_chunk$set(echo = FALSE, message=FALSE, warning=FALSE)
panderOptions('table.split.table', Inf)
```

# Sampling effort distribution
```{r}
sed_by_year <- dat %>%
  group_by(plot_type, sample_year) %>%
  summarise(n=n()) %>%
  ungroup %>%
  mutate(plot_type=factor(ifelse(plot_type=='B', 'Burn', 'Control'), levels=c('Control', 'Burn'))) %>%
  rename(Group=plot_type) %>%
  reshape2::dcast(Group ~ sample_year, value.var='n') %>% 
  rename(Baseline=`0`)

trunc_sed_by_year <- sed_by_year[, -1]
thin_pad <- paste(rep('&nbsp;', 2), collapse='')
thick_pad <- paste(rep('&nbsp;', 10), collapse='')
classes <- names(sed_by_year)[-1]
htmlTable(trunc_sed_by_year,
          header = paste0(thick_pad, classes),
          align.header=paste(rep('r',length(classes)),collapse=''),
          rnames = c('Control', 'Burn'),
          rgroup = 'Study group',
          n.rgroup = 2,
          cgroup = paste0(paste(rep(thick_pad, 3), collapse=''), 
                          'Year post-treatment'),
          n.cgroup = length(classes),
          align='r'
          )

# sed_stats <- dat %>%
#   group_by(plot_type, plot_name) %>% summarise(n=n()-1) %>%  # re-survey events
#   group_by(plot_type) %>% summarise(mean=mean(n), sd=sd(n))  # mean and sd by group
# pander(sed_stats) #justify = 'lrrrr'
```

# Empirical medians and means

The following table reports the empirical medians for each of the responses 
described in the analysis dictionary.


```{r echo=FALSE, message=FALSE, warning=FALSE, fig.align="center"}
var_stats <- function(data) {
 
  trials <- NULL
  if (!is.na(data[['trials_var']])) trials <- data[['trials_var']]
  tmp <- dat %>%
    select_(.dots=c(data[['dependent_var']], 'plot_type', 'sample_year', trials))
  if (!is.na(data[['trials_var']])) {
    # browser()
    mutate_call <- lazyeval::interp(~a / b, a=as.name(data[['dependent_var']]), b=as.name(trials))
    tmp %<>% mutate_(.dots= setNames(list(mutate_call), data[['dependent_var']]))
    tmp <- tmp[, -which(names(tmp)==trials)]
  }
 # browser()  
 tmp %<>%
    filter(sample_year %in% c(0, 20)) %>%  # removed year 15
    mutate(sample_year=ifelse(sample_year==0, 'pre', 'post'),
           sample_year=factor(sample_year, levels=c('pre', 'post'))) %>%
    group_by(plot_type, sample_year) %>%
    reshape2::melt() %>%
    tbl_df %>%
    group_by(plot_type, sample_year, variable) %>%
    summarise_each(funs(mean, median)) %>%
    group_by(plot_type, variable) %>%
    mutate(mean_diff=mean[sample_year=='post'] - mean[sample_year=='pre'],
           median_diff=median[sample_year=='post'] - median[sample_year=='pre']) %>%
    arrange(variable, sample_year)
   
   variable_desc <- formatted_analysis_dict %>% 
     filter(Dependent_var==data[['dependent_var']], Family==data[['family']]) %>% 
     select(Description, Dependent_var, Order)
   tmp %>% 
    left_join(variable_desc, by=c('variable'='Dependent_var')) %>%
    select(Description, Order, plot_type, sample_year,
           dplyr::contains('mean'), dplyr::contains('median'))
}

pre_post_summaries <- analysis_dictionary %>% 
  # slice(5) %>% 
  rowwise %>% 
  do(var_stats(.))


wrapper <- function(x, stat) {
  x1 <- x %>%
    select_(.dots=c('Description', 'sample_year', 'plot_type', stat)) %>% 
    reshape2::dcast(Description~plot_type+sample_year)
  x2 <- x %>%
    select_(.dots=c('Description', 'plot_type', paste(stat, 'diff', sep='_'))) %>% 
    distinct %>% 
    reshape2::dcast(Description~plot_type) %>% 
    rename(B_diff=B, C_diff=C)
  x1 %>% left_join(x2) %>% mutate_each(funs(round(., 3)), -Description)
} 

pre_post_medians <- pre_post_summaries %>%
  group_by(Description, Order) %>%
  do(wrapper(., stat='median')) %>%
  arrange(Order) %>%
  ungroup
pander(pre_post_medians %>% select(-Order), justify = 'lrrrrrr')

```

The following table reports the empirical means for each of the responses
described in the analysis dictionary.

```{r echo=FALSE, message=FALSE, warning=FALSE, fig.align="center"}
pre_post_medians <- pre_post_summaries %>%
  group_by(Description, Order) %>%
  do(wrapper(., stat='mean')) %>%
  arrange(Order) %>%
  ungroup
pander(pre_post_medians %>% select(-Order), justify = 'lrrrrrr')

```

# Other summary statistics

Estimates of the mean decay class of course woody debris (CWD) in burned and control plots at years 15 and 20, where a value of 1 indicates very sound, and 5 very rotten, CWD.

```{r echo=FALSE, message=FALSE, warning=FALSE, fig.align="center"}
decay_class <- dat %>%
  select(plot_type, sample_year, cwd_decay_class) %>%
  na.omit %>%
  filter(cwd_decay_class!=-1) %>%
  group_by(plot_type, sample_year) %>%
  summarise_each(funs(mean, sd)) %>%
  mutate_each(funs(round(., 2)))
pander(decay_class, justify = 'llrr')
```

The mean start date of sampling in each group (along with the standard devation of dates, in days).

```{r echo=FALSE, message=FALSE, warning=FALSE, fig.align="center"}
sampling_init <- dat %>%
  filter(sample_year==0) %>%
  select(plot_type, date_of_first_sample, days_since_start) %>%
  group_by(plot_type) %>%
  summarise(mean=format(mean(date_of_first_sample), '%m/%d/%Y'), sd_dss=sd(days_since_start))
pander(sampling_init, justify = 'lrr')
```

A summary of variables related to biophysical setting.

```{r echo=FALSE, message=FALSE, warning=FALSE, fig.align="center"}
library(purrr)
source(file.path(PROJHOME, 'code', 'analysis', 'helpers.R'))

grp_mukeys <- dat %T>% 
  pipe_assign('all_mukeys', unique(.[['mukey']])) %>%
  select(plot_type, mukey) %>%
  split(.$plot_type) %>% 
  map(~ sort(unique(.$mukey)))
distinct_B_mukeys <- grp_mukeys$B[!grp_mukeys$B %in% grp_mukeys$C]
distinct_C_mukeys <- grp_mukeys$C[!grp_mukeys$C %in% grp_mukeys$B]
  # map(~ paste(sort(unique(.$mukey)), collapse=', '))

biophysical_setting <- dat %>%
  select(plot_type, elevation, northness, slope, tpi_300) %>%  #mukey
  distinct %>% 
  group_by(plot_type) %>% 
  summarise_each(funs(mean, sd)) %>% 
  mutate_each(funs(round(., 2)), -plot_type) %>% 
  left_join(data.frame(plot_type=c('B', 'C'), 
                       unique_mukeys=c(paste(distinct_B_mukeys, collapse=', '), 
                                       paste(distinct_C_mukeys, collapse=', ')))
            )
  
pander(biophysical_setting, justify = 'lrrrrrrrrr')


```
NOTE: shared mukeys include: `r as.integer(all_mukeys[!all_mukeys %in% c(distinct_B_mukeys, distinct_C_mukeys)])`. 

Overstory composition. 
```{r echo=FALSE, message=FALSE, warning=FALSE, fig.align="center"}
# see load_overstory_data.R in `code/data_prep`
# shade_tolerant_spp <- c('ABCO', 'ABMA', 'CADE27')
# shade_intolerant_spp <- c('PICO', 'PIJE', 'PIPO', 'SASC', 'PILA') 
# dat %>%
#   select()


```

Characteristics of trees in small- and large size classes (e.g., shade tolerance).
```{r echo=FALSE, message=FALSE, warning=FALSE, fig.align="center"}
# see load_overstory_data.R in `code/data_prep`
# shade_tolerant_spp <- c('ABCO', 'ABMA', 'CADE27')
# shade_intolerant_spp <- c('PICO', 'PIJE', 'PIPO', 'SASC', 'PILA') 
# dat %>%
#   select()


```
