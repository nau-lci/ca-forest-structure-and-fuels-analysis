# Notes on the structure of the data
## Group, subject, and other structural variables
Key					| Description
--------------------|--------------------------------------------------------------------
`plot_type` 		| Either 'B' or 'C' to designate a burn or control plot, respectively
`plot_name` 		| Plot identification code. The first digit designates the type of plot (F stands for forest). Digits 2-6 contain the species code (the first two letters of the genus and species) plus a unique number for each species, Digit 7 designates the phenology of the vetation at the time of burning (D stands for dormant). Digits 8-9 make up the fuel code using the NFFL fuel modeling system (Anderson 1982). The remaining digits are the unique identifiers for each plot.
`sample_year`		| Year of the study (may be treated as a categorical or a continuous variable)
`transect`			| Available only for fuels data, an indication of the transect along which fuel class counts were collected
`severity`          | Burn severity. A qualitative measurement in which 5 indicates a low burn severity and 1 indicates high burn severity.

## Dependent variables grouped by variable type
Variable type		| Keys
--------------------|------------------------------------------------------------------------------------------------------
Count-based      	| `live_trees_per_ha`, `snags_per_ha`, `large_trees_per_ha`, `small_trees_per_ha`, `spotted_owl_nest_trees_per_ha`, `proportion_shade_intolerant`, `live_tree_qmd`, `one_hour_vol`, `ten_hour_vol`, `hundred_hour_vol`, `total_fine_fuel_vol`, `thousand_hour_vol`
Proportions			| `proportion_large_trees`, `proportion_small_trees`, `proportion_white_fir`, `proportion_pine`, `proportion_shade_tolerant`, `bare_ground_and_rock_cover`, `litter_cover`, `woody_cover`, `live_cover`
(Roughly) continuous| `duff_depth_cm`, `litter_depth_cm`, `fuelbed_depth_cm`

Note: though they appear in the ground cover table, `species_richness` and `species_evenness` are ignored in this analysis
because more suitable data to address composition-related questions exist.

## Notes
First pass: days_since_start was significant for just 1 of 14 variables so it
was dropped from subsequent iterations.
Second pass: if I recall correctly, this model was specified as
`(baseline + plot_type)^2 + northness` and adding northness as a main effect did
not help so it was decided to try it as an interacting effect instead, see below.
Third pass: `(baseline + northness + plot_type)^2`
```
> sig_effects %>% group_by(fixef) %>% tally
# A tibble: 4 x 2
                fixef     n
                <chr> <int>
1            baseline    12
2  baseline:northness     2
3 baseline:plot_typeB     4
4          plot_typeB     7
```
Fourth pass:
