load_package('dplyr')
load_package('ggplot2')
load_package('brms')


results_file <- file.path(PROJHOME, 'results', 'models_df_1_to_10.rds')
# download.file(url='https://www.dropbox.com/s/dzh4vuxoit97daq/models_df.rds?dl=1',
#               destfile=results_file)
results_df <- readRDS(results_file)
results_df

# Response variables.
results_df %>% .[['description']]

# Grab a model from the 'tibble'.

this_model <- results_df %>%
  filter(dependent_var=='trees_per_ha') %>%#litter_depth_cm
  .[['mod_1']] %>% slice_list

# Priors.
get_prior(this_model$formula, this_model$data)

# Model summary.
summary(this_model)

# Launch interactive model evaluation app.
launch_shiny(this_model)
