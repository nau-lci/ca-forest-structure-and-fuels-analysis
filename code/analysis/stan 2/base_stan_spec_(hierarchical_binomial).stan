data {
  // Scalars
  int<lower=1> N;  // number of observations
  int<lower=1> J;  // number of groups
  int<lower=1> K;  // number of covariates, excluding the intercept term

  // Predictors
  real severity[N];
  int<lower=0, upper=1> sample_year1[N];
  int<lower=0, upper=1> sample_year2[N];
  int<lower=0, upper=1> sample_year5[N];
  int<lower=0, upper=1> sample_year10[N];
  int<lower=0, upper=1> sample_year20[N];
  int<lower=1, upper=J> plot_name[N];

  // Response (successes and trials)
  int<lower=0> Y[N];
  int<lower=0> trials[N];
}
parameters {
  // Group-level effects
  real u_0j[J];
  // Standard deviation of group-level effect u_0j
  real<lower=0> sigma_u0;

  // Population intercept
  real beta_0;
  // Population effects
  real beta[K];
}
model {
  // Parameters for the varying intercepts
  real beta_0j[J];

  // Linear predictor
  real lp[N];

  // Priors
  beta ~ normal(0, 10);
  u_0j ~ normal(0, sigma_u0);

  // Varying intercepts definition
  for (j in 1:J) {
    beta_0j[j] = beta_0 + u_0j[j];
  }

  // Individual mean definition
  for(n in 1:N) {
    lp[n] = beta_0j[plot_name[n]] + beta[1]*severity[n] + beta[2]*sample_year1[n] + beta[3]*sample_year2[n] + beta[4]*sample_year5[n] + beta[5]*sample_year10[n] + beta[6]*sample_year20[n] + beta[7]*severity[n]*sample_year1[n] + beta[8]*severity[n]*sample_year2[n] + beta[9]*severity[n]*sample_year5[n] + beta[10]*severity[n]*sample_year10[n] + beta[11]*severity[n]*sample_year20[n];
    Y[n] ~ binomial_logit(trials[n], lp[n]);
  }
}
