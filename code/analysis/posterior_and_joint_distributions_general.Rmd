---
output: html_document
---
  
&nbsp;&nbsp;&nbsp;&nbsp;The first model was built to address three primary research questions. Specifically, we sought to evaluate changes in key forest structure, composition, and fuel variables in: 1) the absence of management intervention over 20 years^[The 'hands-off' approach.]; 2) the presence of a management intervention (i.e., first-entry prescribed fire) over 20 years; and 3) the net effect of treatment 20 years post-implementation. The full (albiet general) model is shown in Figure <mark>XX</mark>. 

<center>
```{r, echo=FALSE, fig.width=5, fig.height=3.5}
library(DiagrammeR)
library(DiagrammeRsvg)
library(magrittr)
library(rsvg)
graph <- '
      digraph mod_1_dag {

# graph, node, and edge definitions
graph [rankdir=BT]
node [shape=plaintext, fontnames="serif"]  // rectangle
edge [arrowhead=vee, arrowtail=none]

# data
y [label=<<I>y</I>>]  //<sub> i</sub>
x [label=<<I>x</I>>]

# individual level
intercept [label=<<I>      ν</I>>]
//intercept [label=<<I>          ν&#7522;</I>>]
betas [label=<<I>β</I>>]
sigma [label=<<I>    σ&#178;</I>>]

# group level
alpha [label=<<I>α</I>>]
sigma_intercept [label=<<I>    ς&#178;</I>>]

# edge definitions with the node IDs
{ rank = sink; x; y; }
{ rank = same; intercept; betas; sigma; }
{ rank = source; alpha; sigma_intercept; }
x -> y [style=dashed, minlen=3]
{intercept betas sigma} -> y
{alpha sigma_intercept} -> intercept

}
'
grViz(graph)

# grViz(graph) %>%
#   export_svg %>% charToRaw %>% rsvg_png(
#     file.path(PROJHOME, 'code', 'analysis', 'generalized_dag.png')
#   )
```
</center>


<!-- <p style="text-align: justify;"> -->
<!-- <span style="font-size:.9em;"> -->
<!-- __Figure <mark>XX</mark>:__ Generalized Bayesian network (_sensu_ Hobbs and Hooten 2015) for the analysis of the effects of time and treatment on an arbitrary forest structure, composition, or fuel variable. Solid lines show stochastic relationships, while dashed lines indicate deterministic relationships. Nodes in the graph are organized hierarchically with data occupying the top row and parameters for individual- and group-level effects in rows 2-3, respectively. All parameters, including subscripts, are defined along with the expression for the posterior and joint distributions (Eq 1). Because the data arise from different distributions, specific  -->


<!-- The parameter $\sigma^2_s$ accounts for uncertainty that arises because our observations are only a sample of all possible instances of the larger population. The true state, $\mu$, is modeled as a linear function of burn severity, sample year, and their interaction, as well as northness, topographic position, and the julian day (since January 1, 1970) on which sampling began for each plot. The parameter $\sigma^2_p$ accounts for uncertainty arising because the process model does not include every possible source of variation in the true state. The group-level model involving $\nu_i$ accounts for variation among plots. Subscripts, e.g., $y_i$, indicate observations (for a given variable) for the $i = 1, ..., n$ plots in the study. Models were set up hierarchically so that clusters of parameters have shared prior distributions (Table <mark>XX</mark>). The specific distribution assigned to each quantity depends on the response variable under consideration, examples of which are provided in Appendix A. -->
<!-- </span>  -->
<!-- </p> -->

<!-- We model the observations tied to each forest structure, composition, or fuel variable as  -->

<!-- \[ -->
<!-- \begin{aligned} -->
<!--   g(\nu_i, \boldsymbol{\beta}, \boldsymbol{x}_i) &= \nu_i + \beta_1x_{1,i} + \beta_2x_{2,i}, ..., + \beta_mx_{m,i}, \\ -->
<!--   \nu_i &= \alpha + \varsigma^2, \\ -->
<!--   ~[\alpha, \varsigma^2, \boldsymbol{\nu}, \boldsymbol{\beta}, \sigma^2_p, \mu, \sigma^2_s~|~\boldsymbol{y}] &∝ \prod_{i=1}^{n}[y_i~|~\mu, \sigma^2_s] \\ -->
<!--   &\times~[\mu~|~g(\nu_i, \boldsymbol{\beta}, \boldsymbol{x}_i), \sigma^2_p] \\ -->
<!--   &\times~[\nu_i~|~\alpha, \varsigma^2] \\ -->
<!--   &\times~[\alpha][\varsigma^2][\boldsymbol{\beta}][\sigma^2_p][\sigma^2_s] -->
<!-- \end{aligned} -->
<!-- \] -->

<!-- where $y_i$ indicate observations (for a given variable) for the $i = 1, ..., n$ plots in the study. Bold, lowercase letters indicate vectors, and lightface lowercase letters, scalars. -->

<!-- &nbsp;&nbsp;&nbsp;&nbsp;[TODO] The second model was developed to evaluate the effects of time and burn severity on forest structure, composition, or fuels variables on treated plots. -->
