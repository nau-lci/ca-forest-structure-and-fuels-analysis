library(rstan)


run_stan_count_data <- function(df, model_config) {
  terms <- as.formula(paste('~', model_config@term_labels))
  mf <- model.frame(terms, df)
  mm <- model.matrix(terms, mf)
  mm_df <- mm %>% as.data.frame

  # Format the data as appropriate for Stan.
  this_data <- list(N=nrow(df),
                    J=n_distinct(df[['plot_name']]),
                    K=ncol(mm)-1,  # '-1' to drop the intercept from count K
                    Y=df[[model_config@dependent_var]],
                    plot_typeB=mm_df[['plot_typeB']],
                    sample_year15=mm_df[['sample_year15']],
                    sample_year20=mm_df[['sample_year20']],
                    plot_name=as.integer(as.factor(df[['plot_name']]))
  )

  # Fit the model.
  if (model_config@family=='poisson') {
    this_code <- file.path(PROJHOME, 'code', 'analysis', 'stan',
                           'base_stan_spec_(hierarchical_poisson).stan')
    these_params <- c('beta_0', 'beta', 'sigma_u0')
  }
  if (model_config@family=='poisson-od') {
    this_code <- file.path(PROJHOME, 'code', 'analysis', 'stan',
                           'base_stan_spec_(hierarchical_poisson-od).stan')
    these_params <- c('beta_0', 'beta', 'sigma_u0', 'sigma')
  }
  if (model_config@family=='negative binomial') {
    this_code <- file.path(PROJHOME, 'code', 'analysis', 'stan',
                           'base_stan_spec_(hierarchical_neg_binom).stan')
    these_params <- c('beta_0', 'beta', 'sigma_u0', 'phi')
  }
  if (model_config@family=='nh-poisson') {
    this_code <- file.path(PROJHOME, 'code', 'analysis', 'stan',
                           'base_stan_spec_(poisson).stan')
    these_params <- c('beta_0', 'beta')
  }
  this_fit <- stan(file=this_code, data=this_data, chains=4,
                   iter=15000, warmup=3000, cores=4,
                   control=list(stepsize=0.01, adapt_delta=0.95), seed=96161)
  # shinystan::launch_shinystan(this_fit)

  return(list(stan_data=this_data, stan_fit=this_fit, stan_pars=these_params,
              model_terms=terms, family=model_config@family))
}
