// Functions.
var aggregate_pixels = function(image, scale) {
  var image_projection = image.projection();
  return image.resample('bicubic').reproject({
      crs: image_projection,
      scale: scale
  });
};
var focal_mean = function(image, scale) {
  return image.reduceNeighborhood(ee.Reducer.mean(), ee.Kernel.circle(scale, 'meters'));
};
var tpi = function(image, from_scale, to_scale) {
  image = aggregate_pixels(image, from_scale);
  return image.subtract(focal_mean(image, to_scale));
};

// Assets.
var lidar = ee.ImageCollection('users/LZachmann/TAHOE_LIDAR')
  .map(function(image) {
    image = image.mask(image);
    var terrain = ee.Algorithms.Terrain(aggregate_pixels(image, 30));
    var northness = terrain.select('aspect').multiply(Math.PI/180).cos().rename(['northness']);
    var tpi_300 = tpi(image, 30, 300).rename(['tpi_300']);
    var tpi_1000 = tpi(image, 30, 1000).rename(['tpi_1000']);
    return terrain
      .addBands(northness).addBands(tpi_300).addBands(tpi_1000)
      .clip(image.geometry().buffer(-1000));
  });
var soils = ee.FeatureCollection('ft:1HmB98KlG5-wiIOS6lByZETSxWrdgYA8E1b-7x5NF')
  .reduceToImage(['MUKEY'], ee.Reducer.first()).rename(['mukey']);
var plots = ee.FeatureCollection('ft:1s-BriQ_Il5lgMgpryG4zG_47qCPMKB78IULmJc_R');  

// Reductions and exports.
var lidar_extent = lidar.geometry().dissolve();
var biophysical_vars = lidar.mosaic().addBands(soils).clip(lidar_extent);
var plot_biophysical_data = biophysical_vars.reduceRegions({
  collection: plots, 
  reducer: ee.Reducer.mean(), 
  scale: 30
});
var selected_attributes = ['plot_type', 'plot_name', 'mukey',
  'elevation', 'northness', 'slope', 'tpi_300', 'tpi_1000'];
Export.table.toDrive({
  collection: plot_biophysical_data.select(selected_attributes), 
  description: 'plot_biophysical_data_export', 
  folder: 'CA State Parks 2', 
  fileNamePrefix: 'plot_biophysical_data', 
  fileFormat: 'CSV'
});
Export.image.toDrive({
  image: biophysical_vars.select(['tpi_300', 'tpi_1000']), 
  description: 'tpi_export', 
  folder: 'CA State Parks 2', 
  fileNamePrefix: 'lidar-based_tpi', 
  region: lidar_extent,
  scale: 30
});

// Mapping.
Map.addLayer(lidar.geometry(), {}, 'Tiles', false);  // Post-clipping to remove edge effects.
Map.addLayer(lidar, {bands: 'hillshade'}, 'Hillshade', false); 
var m_tpi = ee.Image('users/LZachmann/lidar-based_tpi')
  .rename(['tpi_300', 'tpi_1000']);
var tpi_range = m_tpi.reduceRegion(ee.Reducer.minMax(), lidar_extent, 30).getInfo();
var spectral = ['d53e4f', 'f46d43', 'fdae61', 'fee08b', 'ffffbf', 'e6f598', 'abdda4', '66c2a5', '3288bd'];
spectral = spectral.reverse();
Map.addLayer(m_tpi, {min: tpi_range.tpi_300_min, max: tpi_range.tpi_300_max,
  bands: 'tpi_300', palette: spectral}, 'TPI 300');
Map.addLayer(m_tpi, {min: tpi_range.tpi_1000_min, max: tpi_range.tpi_1000_max,
  bands: 'tpi_1000', palette: spectral}, 'TPI 1000');
  
//*/