source(file.path(PROJHOME, 'code', 'global.R'))


ch <- odbcConnect('PostgreSQL35W', 'postgres;Database=FFI')
raw_dat <- sqlQuery(ch, 'select * from get_fine_fuels()')
odbcCloseAll()


test_file(file.path(tests_dir, 'test_data_integrity.R'))  
test_file(file.path(tests_dir, 'test_fuels.R'))


# Dictionary (Brown's terminology to CA State Parks'):
# - 'sample point' == transect
# - 'fuel depth' == litter depth
# - 'plot' == vertical planar projection

fine_fuels <- raw_dat %>% 
  rowwise() %>% 
  # Compute Brown's 'tons per acre' estimate for each record.
  mutate(one_hour_tpa = tons_per_acre(
           one_hour, size_class='one_hour', percent_slope=slope),
         ten_hour_tpa = tons_per_acre(
           ten_hour, size_class='ten_hour', percent_slope=slope),
         hundred_hour_tpa = tons_per_acre(
           hundred_hour, size_class='hundred_hour', percent_slope=slope)) %>% 
  group_by(plot_type, plot_name, sample_year) %>% 
  summarise(one_hour_counts = sum(one_hour),
            ten_hour_counts = sum(ten_hour),
            hundred_hour_counts = sum(hundred_hour),
            one_hour_tpa_mean = mean(one_hour_tpa),
            ten_hour_tpa_mean = mean(ten_hour_tpa),
            hundred_hour_tpa_mean = mean(hundred_hour_tpa)) %>%
  rowwise() %>% 
  mutate(
    one_hour_vol = get_fuel_vol_at_transect(
      one_hour_counts, 0.125 * cm_per_in, 4 * 6 * m_per_ft),
    ten_hour_vol = get_fuel_vol_at_transect(
      ten_hour_counts, 0.625 * cm_per_in, 4 * 6 * m_per_ft),
    hundred_hour_vol = get_fuel_vol_at_transect(
      hundred_hour_counts, 2 * cm_per_in, 4 * 12 * m_per_ft),
    total_fine_fuel_vol = one_hour_vol + ten_hour_vol + hundred_hour_vol) %>% 
  ungroup() %T>% 
  write.csv(., file.path(PROJHOME, 'data', 'developed',
                         'fine_fuels.csv'), row.names=FALSE)
