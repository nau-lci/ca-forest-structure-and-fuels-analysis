# Overview
Building the FFI database begins with two principal components: the raw csv data (located in the data directory) and the physical schema that has been built with Oracle's SQL Workbench 
(file located here: code/data_prep/ffi_physical_model_source.mwd). The raw csv data is parsed and transformed by Python scripts so that all columns containing data are scrubbed for NULL values 
(replaced by -1 for numeric datatypes and a single quote for text datatypes); empty columns are removed; and finally, the columns are formatted for ingestion into an FFI database. If the 
database does not exist, Oracle's Workbench tool can resconstruct the schema via DDL commands and ease data ingestion by building DML commands. Advanced users may even modify the schema so that it more closely
matches their research goals. 

# Building the database

1. Open a Command Prompt window
2. Make sure your python environment is correct by typing in python.exe. If you see >>>, you are ready to go. Type 'quit()' (no quotes), otherwise you will have to modify environment settings to 
reflect the location of python.exe
3. First run [processFFI.py](/processFFI.py) against the [FFI](https://www.frames.gov/partner-sites/ffi/ffi-home/) CSVs in the [data](../../data/) directory.
The syntax is as follows: 
```
python processFFI.py -s <source csv directory> -c <configuration file directory>
```
For example:
```
python processFFI.py -s "c:\ffi\raw csv data" -c "c:\ffi\configuration"
```

If the arguments are left off, the Python code will use the current working directory as the source for both the csv and configuration files.
This will generate an intermediate and local set of files with an *FFI* file prefix. **NOTE**: the tool will push the 'FFI' prefixed files into the source csv
directory. There currently is no option for placing them in a user-defined directory.

4. Build the sampled plot table by running [buildSampledPlot.py](/buildSamplePlot.py) at the command line. To run it, type: 
```
python buildSampledPlot.py <source table name - use the cover csv built in step one> <comparison table name - use any other output csv from step one> <output path and name>
```
For example:
```
python buildSampledPlot.py "c:\ffi\raw csv data\FFIcoverPoints.csv" "c:\ffi\raw csv data\FFItreesIndividuals.csv" c:\ffi\processed_csv\sampledPlotData.csv
```
If you see the error message, "inconsistent number of plots", it means that there are plots in the source table that do not exist in the comparison table (or vice-versa).
The output is a comma delimited text file containing the plot name, the year the plot was sampled and the plot type (control or burn). 

5. Run *buildInsert.py* to create the database insert statements. Its arguments are as follows: 
```
python buildInsert.py <source csv (step 1 or 2) > <insert statements 'insert.sql'> <zero-based index of insert statement> <config file for table> <output path and name>
```
Note: insert.sql is a text file containing the SQL insert commands for populating the tables in the FFI database. Each insert command has an index value as follows: 

    0 - plot data
    1 - sampled plot data
    2 - transect data
    3 - seedling data
    4 - tree data
    5 - thousand hour fuels data
    6 - duff and litter data
    7 - fine fuels data
    8 - cover data

Most users will want to use values 3 - 8 to update the database as corrections and additional data are made available. Plot data (index 0) must be constructed, in part, by
using GIS tools - the details of which are currently beyond the scope of this document.

For example, for the thousand hours fuel table:
```
python buildInsert.py i:\ffi\data\ffisurfacefuels1000hr.csv i:\ffi\code\insert.sql 5 i:\ffi\code\thousand-insert.cfg i:\ffi\code\thousand_insert.sql
```
6. Make sure tables to be updated are empty - delete from `<table name>`
7. Open the sql file from step 3 in a notepad document
8. Open an sql interface window in postgreSQL
9. Copy and paste the sql into the window
10. Repeat steps 2 & 3 as necessary

## Additional notes

*ffi_er_conceptual_model_source.dia* was built with [Dia](https://wiki.gnome.org/action/show/Apps/Dia?action=show&redirect=Dia). *ffi_er_physical_model_source.mwb* was built with [MySQL Workbench](https://dev.mysql.com/downloads/workbench/). Modifying either of these files requires a knowledge of ER principles.

The MySQL Workbench file may be used to generate customized working versions of an FFI database (details of which are beyond the scope
of this document). Configuration files (_*.cfg_) that have the word insert in them are used in steps 2 & 3 above. The other configuration files are used for *processFFI.py*.

# Preparing the CSVs used in the analysis
To create the plot- or even transect-level summaries used in the analysis for statistical inference, use the *load_<response set>.R* family of scripts. 