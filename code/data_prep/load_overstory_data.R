source(file.path(PROJHOME, 'code', 'global.R'))

scale_to_ha <- function(x, hectares=plot_size_ha) {
  return(x / hectares)
}
perc_mortality <- function(ba, yr, from_yr, to_yr) {
  prop_mort <- diff(ba[yr %in% c(from_yr, to_yr)]) / ba[yr == from_yr]
  return(ifelse(prop_mort > 0, 0, prop_mort * 100))
}

shade_tolerant_spp <- c('ABCO', 'ABMA', 'CADE27')
shade_intolerant_spp <- c('PICO', 'PIJE', 'PIPO', 'SASC', 'PILA') 


ch <- odbcConnect('PostgreSQL35W', 'postgres;Database=FFI')
raw_dat <- sqlQuery(ch, 'select * from get_overstory()') %>% 
  resolve_known_issues(., 'trees_data_case_corrections.csv')
odbcCloseAll()


test_file(file.path(tests_dir, 'test_data_integrity.R'))
test_file(file.path(tests_dir, 'test_overstory.R'))


forest_structure <- raw_dat %>% 
  fix_inconsistent_sp_codes(.) %>% 
  mutate(live_tree = ifelse(status=='L', TRUE, FALSE),
         white_fir = ifelse(species=='ABCO', TRUE, FALSE),
         pine = ifelse(species=='PIJE' | species=='PIPO', TRUE, FALSE),
         shade_tolerant = ifelse(species %in% shade_tolerant_spp, TRUE, FALSE),
         shade_intolerant = 
           ifelse(species %in% shade_intolerant_spp, TRUE, FALSE),
         large_size_class = ifelse(dbh >= 60, TRUE, FALSE),
         medium_size_class = ifelse(dbh >=30 & dbh < 60, TRUE, FALSE),
         small_size_class = 
           ifelse(dbh >=15 & dbh < 30, TRUE, FALSE),
         spotted_owl_nest_tree = ifelse(dbh > 76, TRUE, FALSE)) %>% 
  group_by(plot_type, plot_name, sample_year) %>% 
  summarise(trees_per_plot = sum(live_tree),
            snags_per_plot = sum(!live_tree),
            # large_snags_per_plot = sum(large_size_class[!live_tree]),  # NO: dbh measured only on subplot!
            large_trees_per_plot = sum(large_size_class[live_tree]),
            medium_trees_per_plot = sum(medium_size_class[live_tree]),
            small_trees_per_plot = sum(small_size_class[live_tree]),
            spotted_owl_nest_trees_per_plot = 
              sum(spotted_owl_nest_tree[live_tree]),
            white_fir_trees_per_plot = sum(white_fir[live_tree]),
            pine_trees_per_plot = sum(pine[live_tree]),
            shade_tolerant_trees_per_plot = 
              sum(shade_tolerant[live_tree]),
            shade_intolerant_trees_per_plot = 
              sum(shade_intolerant[live_tree]),
            tree_qmd = 
              quadratic_mean_diameter(dbh[live_tree], sum(live_tree)),
            tree_basal_area_per_plot = sum(this_tree_basal_area(dbh[live_tree]))) %>% 
  mutate_each(funs(scale_to_ha), dplyr::matches('_per_plot$')) %>% 
  rename_(.dots=setNames(names(.), 
                         tolower(gsub('_per_plot$', '_per_ha', names(.))))) %>% 
  arrange(plot_type, plot_name, sample_year) %>% 
  mutate(
    mortality_all_trees_yrs_15_to_20 = ifelse(
      sum(sample_year %in% c(15, 20)) == 2, 
      perc_mortality(tree_basal_area_per_ha, sample_year, 15, 20), NA)) %>%
  ungroup() %T>% 
  write.csv(., file.path(PROJHOME, 'data', 'developed', 'forest_structure.csv'), 
            row.names=FALSE)
