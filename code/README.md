# Contents

Children directories (described in the table below) contain code used to prepare, explore, and analyze data. The file *global.R* contains references to R packages, environment settings, constants, lookup tables, and functions that are relevant across data development and analysis tasks. The file *drop_create_and_restore_db.bat* contains command line code that can be used in place of pgAdmin III to restore the database from the database backup file. 

Directory                 		| Description
----------------------------------------|-------------------------------------------------------------------------------------------------------
[**analysis/**](analysis/)         		| R code used to make Bayesian statistical inference from the data
[**data_exploration/**](data_exploration/) 	| Code used to summarize the main characteristics of the data
[**data_prep/**](data_prep/)       	 	| A collection of code used to populate the database and plot-level summaries used in this analysis
[**tests/**](tests/)  	  		| Tests used to confirm that the data is fit for use (i.e., is structured according to our expectations)