context('Fuels data structure')

test_that('There are fuels counts for 4 transects at each sampling event', {
  transects_per_event <- raw_dat %>% 
    group_by(plot_type, plot_name, sample_year) %>% 
    summarise(n=n_distinct(transect)) %>% 
    .$n 
  expect_true(all(transects_per_event==4))
})
