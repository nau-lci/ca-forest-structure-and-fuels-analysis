# Packages, helpers, etc.
library(rstan)
library(RODBC)
library(testthat)
library(dplyr)
library(magrittr)
library(reshape2)
library(rgdal)
library(RColorBrewer)
library(ggplot2)
library(pander)
source(file.path(PROJHOME, 'code', 'functions.R'))
source(file.path(PROJHOME, 'code', 'constants.R'))
source(file.path(PROJHOME, 'code', 'data_exploration', 'utils.R'))


known_issues_dir <- file.path(PROJHOME, 'data', 'known_issues')
tests_dir <- file.path(PROJHOME, 'code', 'tests')

# Lookup tables
ch <- odbcConnect('PostgreSQL35W', 'postgres;Database=FFI')
spp_dat <- sqlQuery(ch, 'select * from get_spp()') %>% tbl_df()
plot_dat <- sqlQuery(ch, 'select * from get_plots()') %>% tbl_df()
odbcCloseAll()
