# A brief public-service announcement...
Only users or contributing developers who need access to the raw (unaggregated) FFI data would need to complete the following installation and configuration instructions. Almost all other analytical needs can be met by simply by loading the data available in the [**data/developed/**](../data/developed/) directory.  

## Table of Contents
- [Guide to the forest structure and fuels database](#markdown-header-guide-to-the-forest-structure-and-fuels-database)
    * [Setup](#markdown-header-setup)
        * [PostgreSQL](#markdown-header-postgresql)
		* [Building the non-spatial database](#markdown-header-building-the-non-spatial-database)
        * [ODBC drivers for Windows](#markdown-header-odbc-drivers-for-windows)
        * [R](#markdown-header-r)
        * [ODBC drivers for R](#markdown-header-odbc-drivers-for-r)
    * [Running a stored procedure in R](#markdown-header-running-a-stored-procedure-in-r)

# Guide to the forest structure and fuels database
We used PostgreSQL to create a single, organized relational database of various nonspatial datasets (e.g., 
coarse and fine surface fuels, and over- and understory data). Spatial data may be added in the future.

## Setup

In order to call functions in Postgres from R, you must first download, install, and configure the following 
applications to create a Postgres-enabled statistical programming environment. In each case, please choose 
the installers the reflect the operating system and address space that it uses (i.e., 32- or 64-bit). Setup 
instructions are for Windows, though all of the software referred to below can be installed on Linux or OS X.

### PostgreSQL
Download and install [PostgreSQL](http://www.postgresql.org/), the Relational Database Management System. 
During the installation:

* Choose a superuser password and write it down;
* Accept default settings until the final screen; and
* Uncheck Launch Stack Builder and finish the installation.

#### Building the non-spatial database
Open pgAdmin III (located in the Postgres program folder), right-click on _Databases_, and choose _New Database..._. 
Use the name 'FFI' and click _OK_. Then right-click the new _FFI_ database in the databases tree and select _Restore..._. 
Browse to the '.backup' file in the repository data directory and hit _Restore_. 

![alt text](../docs/screenshots/database_config_1.png)

Alternatively, the database can be built from the command line by adapting the code in drop_create_and_restore_db.bat.

### ODBC drivers for Windows
Download and install [psqlodbc](http://www.postgresql.org/ftp/odbc/versions/msi/). Open your data sources component 
(Control Panel > Administrative Tools). Click on _Add_. Note: if you'd like to configure the database to be available 
to all users on a machine, use the _System DSN_ tab rather than the _User DSN_ tab. 

![Alt text](../docs/screenshots/odbc_driver_setup_1.png)

Choose the PostgreSQL Unicode driver and click _Finish_.

![alt text](../docs/screenshots/odbc_driver_setup_2.png)

Enter the database name as it appears in the pgAdmin III administrative panel. Enter _localhost_ in the server field 
and _postgres_ as the user name. Use the superuser password generated in the PostgreSQL installation step above. 

![alt text](../docs/screenshots/odbc_driver_setup_3.png)

Test the configuration be pressing _Test_. If all is well, you should see the words 'Connection successful'. Make a 
note of the string in 'Data Source', as this will be used later on to establish a connection to the database in R. 
Press _Save_ and exit out of the ODBC Data Source Administrator by pressing _OK_.

### R
Download and install the latest version of [R](https://www.r-project.org/).

### ODBC drivers for R
Launch R and install the 'RODBC' package by running:

```r
install.packages('package_name')
```

## Running a stored procedure in R
Available functions (stored procedures) are described in the following table:

Procedure | Component | Description | Units | Special values
- | - | - | - | -
`get_duff_litter()` | Surface fuels | Retrieves all duff and litter records for plots (40 individual measures per plot per sampling event prior to 2010, 16 afterwards) | Inches |
`get_fine_fuels()` | Surface fuels | Retrieves 1-, 10-, and 100-hour fuel counts for each transect (_n_ = 4) within each plot. Transect lengths and midpoint diameters for each fuel class are also provided. | Feet (for transects) and inches (for fuel diameters) |
`get_thousand_hour_fuels()` | Surface fuels | Retrieves the diameters of all coarse woody debris along fuel transects in each plot | Inches |
`get_overstory()` | Forest structure | Retrieves the species identity and diameter at breast height (DBH) of both live and dead overstory trees. Mature, overstory trees were counted on the full 20m X 50m plot. | Inches | -1 (null DBH)
`get_seedling()` | Forest structure | DEPRECATED. Retrieves the counts of seedlings (by height class) on 5m X 10m nested subplots. |  | 
`get_ground_cover()` | Forest structure and community composition | Retrieves vegetation and substrate records sampled along 50-m transects, with points spaced every 30 cm (_n_ = 166 points/transect) |  | 
`get_plots()` | Metadata | Retrieves basic plot information, including location (latitude, longitude, state park), dominant tree type, burn year and severity, etc. | Mixed |
`get_spp()` | Metadata | Retrieves the lookup table used to obtain species scientific names from species codes

As a result of an idiosyncracy of the original FFI database system,
some records that should be present in the raw data tables are currently missing. An example of one such issue involves 
coarse woody debris (CWD) records for control plot FPIJE1D05-04 during the pre-treatment data collection event. Crews visited
this plot and observed 0 CWD fuels along all transects. However, it was not possible to enter such records into FFI, so they 
appear to be missing. As such, we devised an _ad hoc_ solution to this missing records problem using a helper function in R. 
Effectively, the helper (xxx.R) identifies these 'lost' records and populates the tables with the informative zeros on the fly.

Compliance of the data to expectations regarding the number of samples, etc. is evaulated using R's 'testthat' package.

The stored procedures can be called from within R as follows:

```R
library(RODBC)

ch <- odbcConnect('PostgreSQL35W', 'postgres;Database=FFI')
raw_dat <- sqlQuery(ch, 'select * from get_fine_fuels()')
odbcCloseAll()
``` 