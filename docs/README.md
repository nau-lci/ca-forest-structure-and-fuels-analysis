﻿# Reporting / manuscript-development

We are using Google Docs to coordinate contributions to reporting on the analysis. We expect to submit a manuscript to [_Forest Ecology and Management_](http://www.journals.elsevier.com/forest-ecology-and-management/). 
Accordingly, we have created three separate documents:

* a [cover letter](https://docs.google.com/document/d/1HvOweA5TeQtFIWW6LJvQZviU4YZw-lPLKmewc58Drh0/edit?usp=sharing);
* the [manuscript](https://docs.google.com/document/d/18X7g_ufzMuByxi2ar5wK42vALuiYvThO2N_1MK2oiCQ/edit?usp=sharing) itself; and
* a [highlights](https://docs.google.com/document/d/1gFuWafSfNQ2NstoeJkrODJ1Gzp5_Ffh5dLPBjeiW4yo/edit?usp=sharing) section. 

Access to the documents can be requested from the owner.

