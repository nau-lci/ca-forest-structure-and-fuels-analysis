

##Database Design – A Guide to Best Practices##



##Table of Contents

####[Foreword](#fw)

####[I. Database Primer](#dbpnf)<br>
 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[a. Normal Forms](#dpnf) <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[b. Entities](#o2m)<br>
 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[i. One-to-Many](#o2m)<br>
 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[ii. Many-to-Many](#m2m)<br>
 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[iii. One-to-One](#o2o)<br>
####[II. Plot Design](#pd)

####[III. Final Considerations](#fc)<br>

####Foreword<a name="fw"></a>

Databases are information storage technologies whose primary design feature is
an assemblage of tables containing stored data. Moreover, these tables have
explicit identifiers, relationships, and cardinalities that describe how they
interact with each other. This kind of classification of data stores allows for
powerful analytical techniques to be applied, the results of which can guide
policy, provide new scientific insight, and, more generally, enhance the
knowledge that has been acquired by both individuals and institutions.
However, as with any tool that processes data, GIGO (Garbage In, Garbage Out)
is always a concern. Schemas that are not thoughtfully designed are
particularly vulnerable to GIGO. This document is an attempt to educate and address some
of the pitfalls that can grind analysis to a halt because of design errors.
Toward this end, this document has been organized into 3
topics – the first is a primer on database design principles, the second applies those
principles to a simplified experimental field design, and the last offers a
brief sketch of techniques to avoid some of the hazards that have been encountered in real-world
designs.

####Database Primer – Normal Forms<a name="dpnf"></a>

As noted in the foreword, databases typically contain an assemblage of tables
that are roughly analogous to worksheets in a spreadsheet program (like
Excel). What makes them unique from spreadsheets, however, is the application
of rules derived from set theory that guarantee that each row in table can be
uniquely identified and associated with data in other tables. Two sample rows
from a table (with its associated header) are shown below:

<u>PLOT ID</u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;XCENTER
&nbsp;&nbsp;&nbsp;&nbsp;YCENTER
&nbsp;&nbsp;&nbsp;&nbsp;AZIMUTH<br>256
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
35.323
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
113.24
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
110<br>
257
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
34.142
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;110.12
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
156

The first thing to note is that PLOT ID is underlined. This is a convention
that denotes a primary key<sup>[1](#[1])</sup> – a key that uniquely identifies the subsequent
pieces of data in that row. In more formal terms, this row is in 2nd normal
form. Without that key, the data loses its uniqueness – one row cannot be
distinguished from another row. Furthermore, the data cannot be joined to
other tables.

It should be noted that these rows are also in 1st normal form. This
normalization requires that a row of data contains values that are atomic in
nature. An example will clarify what this means:

<u>ADDRESS ID</u>&nbsp;&nbsp;&nbsp;&nbsp;
ADDRESS<br>
234 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
6 N MAIN ST ANYWHERE,MN 83201

In this example, we have ADDRESS ID as the primary key and ADDRESS as the
associated dependent data. Notice that ADDRESS is a composite value – street
address, city, state, and zip code. It is not atomic and therefore not in 1st
normal form. Properly normalized, ADDRESS would look like this:

<u>ADDRESS_ID</u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
STREET &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
CITY &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
STATE&nbsp;&nbsp;&nbsp;
ZIP<br>
234 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
6 N MAIN ST&nbsp;
ANYWHERE&nbsp;&nbsp;&nbsp;
MN&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
83201

Notice that STREET cannot be further decomposed – removing any piece of the
address renders it unusable as an address. The same goes for city, state and
zip.

The next normalization form - 3rd normal form \- is designed to remove hidden
dependencies in a row of data. Again, an example will clarify:

<u>PLOT ID</u>&nbsp;&nbsp;&nbsp;&nbsp;
XCENTER&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
YCENTER&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
AZIMUTH&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
SUBPLOT ID&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
SUBPLOT AREA<br>
256&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;35.323&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;113.24&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;110&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5

In this row, the hidden dependency is the SUBPLOT AREA field being directly
dependent on SUBPLOT ID. If SUBPLOT ID is removed, we have no information
about which SUBPLOT ID the SUBPLOT AREA belongs to. Resolving this requires
the creation of a plot and subplot table:<br>
<u>PLOTID</u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;XCENTER&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;YCENTER&nbsp;&nbsp;&nbsp;&nbsp;AZIMUTH<br>256&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;35.323&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;13.24&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
110

![](../ddbp_files/image001.png)

PLOT ID &nbsp;&nbsp;&nbsp;<u>SUBPLOT ID</u>&nbsp;&nbsp;&nbsp;&nbsp;SUBPLOT AREA<br>
256&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
5

The new table now contains two primary keys that bind the SUBPLOT AREA to the
correct SUBPLOT ID and PLOT ID columns.  It is important to note that PLOT ID
is now a _**foreign key**_ (FK) of the subplot table (arrow).  The foreign key
allows data from the subplot table to be associated with the correct plot
table. (See discussion under Database Primer – Entities.)

When these three normal forms are applied to a database schema, referential
integrity of the database is established. In practical terms, it means that
errors resulting from data duplication are eliminated and transformations
(insert, delete, update) can be applied to the data, in whole or part, without
concern for database corruption.<sup>[2](#[2])</sup>

####Database Primer – Entities<a name="o2m"></a>

No discussion of database fundamentals can be complete without discussing
entities. Entities are at the core of any database schema. Entities are
typically real world objects that have attributes attached to them. E.g. an
airplane is an entity that has, among other attributes, an FAA registration
number, a maximum operating altitude, a minimum takeoff velocity and a seat
count.

In ER<sup>[3](#[3])</sup> database design notation, the entity would look something like this:

![](ddbp_files/image003.jpg)

Note that the primary key is identified as the FAA registration number.
The corresponding table design would look like this:

<u>REGISTRATION NUM</u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TAKEOFF VELOCITY&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MAX ALTITUDE&nbsp;&nbsp;&nbsp;SEAT COUNT<br>
XF1567&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;152&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;45000&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;150

A complementary entity would be the passenger entity. Potential attributes for
a passenger entity would be passenger ID, passenger last name, passenger first
name, passenger gender.

![](ddbp_files/image005.jpg)

The corresponding table:

<u>PASSENGER ID</u>&nbsp;&nbsp;&nbsp;&nbsp;LAST NAME&nbsp;&nbsp;&nbsp;&nbsp;FIRST NAME&nbsp;&nbsp;&nbsp;GENDER<br>
588964&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DOE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;JOHN&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;M

And finally the relationship type that “glues” these two entities together:

![](ddbp_files/image007.png)

The relationship type typically does not have a corresponding table entry<sup>[4](#[4])</sup>
and may be viewed as a pedagogical tool to help understand the kind of
_**cardinality**_ that exists between entities.

Putting these pieces together yields this final diagram:

![](ddbp_files/image011.jpg)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
![](ddbp_files/image009.png)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;![](ddbp_files/image008.png)

This diagram contains several new features (referenced by the arrows) that
indicate _cardinality._ In this case, on the left-hand side, we have a single
instance of the airplane entity, denoted by the vertical mark, and, on the
right-hand side we have multiple instances of the passenger entity, denoted by
the _**crows’s feet**._ This one-to-many relationship is a typical relationship
between entities. In fact there are a number of cardinalities available for
design purposes:

_One-to-many (1:M)_

_Many-to-many (M:N)_

_One-to-one (1:1)_

To continue with our airplane example as a 1:M relationship, consider a single
passenger – she can only _fly_ on one airplane at a time (she cannot clone
herself, immediately ruling out a many-to-many relationship – see discussion
below), so the plane entity is limited to a single instance (either 1:M or
1:1). In entity-relationship language, a single passenger is _flown by_ a
single plane. Now let’s consider a single passenger plane – it can transport
any number of passengers, up to its design limits; this immediately eliminates
the 1:1 relationship, as we have a multiplicity of passengers. We are thus
left with the 1:M relationship – a single airplane _flies_ one or more
passengers.

The only required change in the table design is to add the airplane PK to the
passenger table as a foreign key. In ER speak: the primary key migrates to the
many-sided table:

<u>PASSENGER ID</u>&nbsp;&nbsp;&nbsp;&nbsp;REGISTRATION NUM&nbsp;&nbsp;&nbsp;LAST NAME&nbsp;&nbsp;&nbsp;
FIRST NAME&nbsp;&nbsp;&nbsp;
GENDER<br>588964&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
XF1567&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DOE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;JOHN&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;M

Now that REGISTRATION NUM is a foreign key, it can be given null values, which
allows passengers to _not_ be assigned to a particular airplane (they haven’t
booked a flight yet, for example).

The many-to-many relationship describes entities where there are multiple<a name="m2m"></a>
associations in both directions. As an example, a single patient may be seen
by one or more doctors and a doctor may see one or more patients:

![](ddbp_files/image015.jpg)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;![](ddbp_files/image013.png)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;![](ddbp_files/image012.png)

Notice that the multiplicity symbols – the _crow’s feet_ – now appear on both
sides of the relationship. We can more easily visualize this relationship with
a table:

 ![](ddbp_files/image017.jpg)

Patient Gilmore sees Drs. Gupta and Smith, while patient Anderson sees Drs.
Gupta and Johnson. Patient Karsten just sees Dr. Smith. Turning this around,
Dr. Gupta sees patient Gilmore and Anderson… and so on.

The table design for this ER diagram:

<u>PATIENT ID</u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LAST NAME&nbsp;&nbsp;&nbsp;FIRST NAME&nbsp;&nbsp;&nbsp;GENDER<br>
253&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GILMORE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GRAPE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;M<br>
453&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;KARSTEN&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;KIMBERLY&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;F<br>
642&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ANDERSON&nbsp;&nbsp;POUL&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;M

<u>DOCTOR ID</u>&nbsp;&nbsp;&nbsp;&nbsp;
LAST NAME&nbsp;&nbsp;&nbsp;FIRST NAME&nbsp;&nbsp;&nbsp;&nbsp;SPECIALTY<br>
5632&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GUPTA&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GANIL&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SURGERY<br>
6431&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SMITH&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PETER&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PODIATRY<br>
7688&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;JOHNSON&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ABLE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PSYCH

A careful reader might note that this doesn’t quite work – there is no
association between the patient and doctor tables, as there is no shared key.
To capture this missing feature, relationship “Sees” must be transformed into
an _associative entity_:


![](ddbp_files/image021.jpg)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;![](ddbp_files/image019.png)
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;![](ddbp_files/image018.png)

With the associative entity, the crow’s feet are moved from the outer entities
to the inner relationship and relationship transforms into an entity.<sup>[5](#[5])</sup> An
additional table is added to the patient and doctor tables:

<u>PATIENT ID</u>&nbsp;&nbsp;&nbsp;<u>DOCTOR ID</u><br>
253&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5632<br>
253&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;6431<br>
453&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;6431<br>
642&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5632<br>
642&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;7688<br>

![](ddbp_files/image022.jpg)

The associative entity simply contains the PKs of the associated tables, in
this case DOCTOR ID and PATIENT ID. It can be verified that the table captures
the relationships identified in the figure below it. Furthermore, additional
metadata may be captured. In this case a DATE VISITED field may be
appropriate:

<u>PATIENT ID</u>&nbsp;&nbsp;&nbsp;&nbsp;<u>DOCTOR ID</u>&nbsp;&nbsp;&nbsp;&nbsp;<u>DATE VISITED</u><br>
253&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5632&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4/22/13<br>
253&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;6431&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;11/22/15<br>
453&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;6431&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;7/7/11<br>
642&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5632&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2/1/10<br>
642&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;7688&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;12/5/09<br>

Notice that this extra field is also a PK (it must be) – the reason for this
is left as an exercise for the reader.

The last kind of entity relationship is the simplest - one entity is related<a name="o2o"></a>
to one and only one other entity. Consider a driver operating a car. In nearly
all cases, a car can only have one driver at a time, and a driver can only
operate one car at a time. If this were not the case, absurdities would
result. A 1:M relationship would imply that either a single driver can operate
any number of cars simultaneously or a single car can be driven simultaneously
by any number of drivers. (N.B. this is context dependent – a 1:M relationship
may be reasonable if simultaneity is dropped, for example).



The ER diagram of this relationship looks like this: 
![](ddbp_files/image024.png)Both sides of the relationship show a single
entity – only one instance of each side will be created.

Transforming the ER diagram to tabular form yields this construction:

<u>CAR VIN</u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DRIVER LIC&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MODEL&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;COLOR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;YEAR<br>
X31235&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DL32532&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TRABANT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BLEAK GREY&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1950<br>

<u>DRIVER LIC</u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LAST NAME&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FIRST NAME&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GENDER&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>
DL32532&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DOE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;JANE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;F&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>

Notice that the primary key of the driver – the dependent entity – migrates to
the car table as a foreign key and does not participate directly in uniquely
identifying the row (this allows a car to have no driver).

####Plot Design<a name="pd"></a>

Now that the fundamentals of database construction are out of the way, an exploration of database design to capture data from experimental fieldwork is now possible. The most important property of experimental field designs is that they tend to have nested features: multiple transects are embedded inside plot boundaries, or a plot is divided into subplots. This naturally gives way to a 1:M relationship. The figure below offers one possible design:

![](ddbp_files/image026.jpg)

It consists of a plot (outer square), subplots (sub-divided squares) and
square transects embedded in each subplot. The numbers serve as PKs (see
discussion below).

An ER diagram of this plot design:

![](ddbp_files/image028.png)

Notice that an additional table now participates in the relationship. N.B.
this design can be extended further to the right if necessary.

As noted previously, the primary key migrates to the many-sided table. In this
instance, the transect table will contain not only its own key, but also the
PKs from the subplot and plot tables. The subplot table, in turn, will contain
the plot PK as well as its own key.<sup>[6](#[6])</sup>

The corresponding table design is shown below:

<u>PLOT ID</u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;XCENTER&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;YCENTER&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PLOT TYPE<br>
1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;34.323&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;101.36&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONTROL<br>
2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;35.653&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;100.72&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BURN<br>


<u>SUBPLOT ID</u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<u>PLOT ID</u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AREA&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>
1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>
2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>
3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>
4&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>


<u>TRANSECT ID</u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<u>SUBPLOT ID</u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<u>PLOT ID</u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AZIMUTH<br>
1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;180<br>
2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;180<br>
3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;180<br>
4&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;180<br>


The metadata given for each table has been limited to one column to simplify
the discussion – however, in practice, enough metadata should be included to
help answer the research question(s) at hand. As an example, the plot table
might include elevation, slope, insolation, aspect and any other variables
that are relevant for analysis.<sup>[7](#[7])</sup>

To capture the biological data, more 1:M additions need to be made to the
above entities. As an example, capturing plot-wide, non-seedling tree data
results in this design:

![](ddbp_files/image030.png)

And the respective table design (with PK migration):

<u>PLOT ID</u>         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;XCENTER&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;YCENTER&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PLOT TYPE<br>
1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;34.323&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;101.36&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONTROL<br>

<u>TREE ID</u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<u>PLOT ID</u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AZIMUTH&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DISTANCE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DBH<br>
1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;110&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;7.8&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0.8<br>
2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;30&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;8&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1.8<br>
3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;135&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0.2<br>
4&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;315&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5.5&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0.7<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>

Each tree is uniquely identified as belonging to plot 1, with the associated
metadata tracking tree location data (azimuth and distance from the centroid)
and DBH providing the relevant biological data. The tabular data graphed onto
the plot would look something like this:

![](ddbp_files/image032.jpg)

Seedling data may be captured within the transect entity:

![](ddbp_files/image034.png)

And the corresponding tables:

<u>TRANSECT ID</u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<u>SUBPLOT ID</u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<u>PLOT ID</u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AZIMUTH<br>
1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;180<br>
2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;180<br>


<u>SEEDLING ID</u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<u>TRANSECT ID</u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<u>SUBPLOT ID</u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<u>PLOT ID</u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SPP&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;COUNT<br>
1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ABCO&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;22<br>
2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PIED&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;11<br>
1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ABLA&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;9<br>

It may be noted that SPP may actually be used as a PK as it allows the row to
be uniquely identified:

<u>SPP</u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<u>TRANSECT ID</u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;COUNT<br>
ABCO&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;22<br>
PIED&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;11<br>
ABLA&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;9<br>

This is a suspect design choice as the PK should be biased towards keys that
don’t map back to the data (they should be data neutral).

Putting it all together yields this final ER diagram:

![](ddbp_files/image036.png)

It gets busy rather quickly – even with this relatively simple design.
However, this ER diagram can be fully implemented in a database system with
the guarantee that it will handle the transactions that are put to it, without
becoming inconsistent. Advanced database design techniques can further
streamline the entity relationships here (subtyping / supertyping) but that
discussion is beyond the scope of this document.

**Final Considerations**<a name="fc"></a>

Database design is a topic worthy of study by scientists – especially those
who are tasked with building a data infrastructure for field collected data. A
reasonably well-designed database will translate into savings of time, energy,
and sanity for all concerned.

Here are some lessons learned from prior work:

DO use simple, stable primary keys – an auto-incremented<sup>[8](#[8])</sup> index is a
reasonable candidate. Not following this rule may result in a fractured
database. A real world example: folding the name of the dominant species of a
plot into a primary key. This is a poor design choice primarily because the
dominant species may change, resulting in an aliased plot, i.e. one that
contains the former dominant species and a new one with the current dominant
species. This data should have been placed in a table.

DO build a data dictionary containing the variables of interest, their ranges,
and the form of NULL or bad data entries. A typical problem that shows up in
analysis is an empty value. Can it be assumed to be zero? Was it collected to
begin with? Is it a mistake?
Identifying these problematic forms with values outside the expected range can
be a huge help. Ex.: DBH can never be negative, so one may decide that a particular
negative value may be used to populate the record in question:

       -1 - field data is missing from datasheets
       -2 - original datasheets contained improbable values, need to remeasure
       -3 - not available due to exceptional circumstance – ex. a crown fire destroys the mature trees in a plot  



DO use variable names that are text and underscore only. Err on the side of
verbosity - it’s easier to remember whole words than their contracted forms.
Use **solar\_insolation** instead of **slr\_ins** as a variable name. Lower
case is preferred, but do choose one or the other and keep it consistent.

DO consider techniques for identifying off-by-one (or more) errors. If the
transect count for a given plot is an even number, then the total count of all
transects in all plots must be even. In computing, CRC values are used to
validate the integrity of data. Maybe something similar can be done with field
data.

DO use dynamic entities where possible / necessary. Associative entities are
powerful constructs for building datasets whose total count is not known a
priori. Unfortunately they don’t get used or they get used incorrectly. A real
world example: a database attempting to associate a primary investigator with
a particular project, simply repeated columns for the primary investigator and
the associated project three times:

  **PROF NAME**&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**PROJECT NAME**&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**PROF NAME2**&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**PROJECT NAME2**&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**PROF NAME3**&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**PROJECT NAME3**<br>
J. Hilliard&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Arthropod Mortality&nbsp;&nbsp;T. Backus&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Climate Change&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;M. Maples&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Lion Migration<br>

This kind of repetition is a sign that the ER design is deficient and that a
dynamic structure (like an associative entity) needs to be considered
(exercise left to the reader).

DO consider creating a preliminary design and evolving it over time. Given
budget and time constraints this may not be a feasible option. (Author note:
in lieu of evolving a design for a recent project, I built a design upfront
after considering all of the metadata I could assemble. It was an imperfect
design; however, it did allow the project to move forward. Ultimately the
design was replaced near the end of the project – at that point I was
intimately familiar with how the pieces of data fit together and understood
where the original design was wanting.)

Lastly, if the process of building a database seems confusing and frustrating
at first, you are not alone. It is as much an art as it is science and relying
on an experienced practitioner is a far better solution than relying on your
instincts about how to proceed. In short, don’t be afraid to ask.

* * *

<a name="[1]">1</a> A primary key is also known by its shorthand notation - PK

<a name="[2]">2</a> Note: Normal forms 4,5,6 are omitted to keep the discussion from becoming unwieldy. For most schemas, NF3 is a sufficient degree of normalization.

<a name="[3]">3</a> ER – Entity Relationship

<a name="[1]">4</a> Relationship types that have assigned attributes or are associative
entities will have an accompanying table

<a name="[5]">5</a> The verb that identifies the relationship may be transformed as well – the
verb becomes a gerund.

<a name="[6]">6</a> Given the nature of field designs, the parts that compose the whole are generally not optional. When the PK migrates to the many-side, it must become a primary key for that side; it is _not_ a simple foreign key. The nested plot design shown above actually represents a de facto subtype/supertype relationship.

<a name="[7]">7</a> There are no hard and fast rules here: many attributes may be derived on
the fly from a GIS system, and thus excluded from the database proper. It all
depends on the context(s) in which the database will be used.

<a name="[8]">8</a> Auto-incremented primary keys are key values generated by the _database_ –
no user input needed

